#!/bin/bash

# ask if he want to install Rudra linux
read -p "Do you want to continue with Rudra Linux? (Y/n): " answere
[ -z "$answere" ] && answere="yes" #set default answere to Yes

rep_mod=$(echo "$answere" | tr '[:upper:]' '[:lower:]') #Convert asw to lower

if [[ "$rep_mod" == "yes" || "$rep_mod" == "y" ]]; then
    rudra_linux=true 
    echo "OKKKK, let's go"

    break
elif [[ "$rep_mod" == "no" || "$rep_mod" == "n" ]]; then
    echo "Action void, keeping your Linux :)"

    break
else
    echo "Unknown answer. Please enter Yes or No."
fi







#Template question.
while true; do
    read -p "<Question>: " answere
    [ -z "$answere" ] && answere="yes" #set default answere to Yes
    
    rep_mod=$(echo "$answere" | tr '[:upper:]' '[:lower:]') #Convert asw to lower
    if [[ "$rep_mod" == "yes" || "$rep_mod" == "y" ]]; then
        <les actions en cas de oui>

        break
    elif [[ "$rep_mod" == "no" || "$rep_mod" == "n" ]]; then
        <Action en cas de non>

        break
    else
        echo "Unknown answer. Please enter Yes or No."
    fi
done





# Variation 2
#!/bin/bash

# Poser la première question à l'utilisateur
read -p "Do you want to continue with Rudra Linux? (Y/n): " continue

# Stocker la réponse dans une variable
if [[ "$reponse1" =~ [yY]|[yY][eE][sS]|"" ]]; then
    continur=true
elif [ "$reponse1" =~ [nN]|[nN][oO]]; then
    action1=false

fi

# Poser la deuxième question à l'utilisateur
read -p "Do you want to perform an additional action? (Y/n): " reponse2

# Stocker la réponse dans une variable
if [[ "$reponse2" =~ [yY]|[yY][eE][sS]|"" ]]; then
    action2=true
else
    action2=false
fi

# Exécuter des scripts en fonction des réponses
if [ "$action1" = true ]; then
    ./script1.sh
fi

if [ "$action2" = true ]; then
    ./script2.sh
fi

# Ajoutez autant de conditions et d'exécutions de scripts que nécessaire en fonction des réponses.
