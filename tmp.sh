while true; do
    read -p "Do you want to continue with Rudra Linux? (Y/n): " answere
    #keep Yes as default answere
    [ -z "$answere" ] && answere="yes"

    rep_mod=$(echo "$answere" | tr '[:upper:]' '[:lower:]') #Convert asw to lower
    if [[ "$rep_mod" == "yes" || "$rep_mod" == "y" ]]; then
        echo "OKKKK, let's go"

        break
    elif [[ "$rep_mod" == "no" || "$rep_mod" == "n" ]]; then
        echo "Action void, keeping your Linux :)"

        break
    else
        echo "Unknown answer. Please enter Yes or No."
    fi
done