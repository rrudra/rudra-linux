alias ll='ls -l'
alias la='ls -lA'
alias l='ls -CF'

#mise a jour
alias aptupdate='sudo nala update && sudo nala upgrade -y'
alias updatefla='flatpak uninstall --unused -y && flatpak update -y'
alias maj='aptupdate && updatefla'

#python
alias python='/usr/bin/python3'

#app
alias code='flatpak run com.visualstudio.code'
